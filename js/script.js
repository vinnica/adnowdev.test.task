$(document).ready(function () {

    var dy = new Date(),
        Year = dy.getFullYear();
    $('span.year').html(Year);


    function updater(h, m, s, h2, m2, s2) {
        var baseTime = new Date(2018, 2, 9);
        var period = 24 * 60 * 60 * 1000;

        function update() {
            var cur = new Date();

            // сколько осталось миллисекунд
            var diff = period - (cur - baseTime) % period;

            // сколько миллисекунд до конца секунды
            var millis = diff % 1000;
            diff = Math.floor(diff / 1000);

            // сколько секунд до конца минуты
            var sec = diff % 60;
            if (sec < 10) sec = "0" + sec;
            diff = Math.floor(diff / 60);

            // сколько минут до конца часа
            var min = diff % 60;
            if (min < 10) min = "0" + min;
            diff = Math.floor(diff / 60);

            // сколько часов до конца дня
            var hours = diff % 24;
            if (hours < 10) hours = "0" + hours;

            // var days = Math.floor(diff / 24);
            //
            // d.innerHTML = days;
            h.innerHTML = hours;
            m.innerHTML = min;
            s.innerHTML = sec;
            h2.innerHTML = hours;
            m2.innerHTML = min;
            s2.innerHTML = sec;

            // следующий раз вызываем себя, когда закончится текущая секунда
            setTimeout(update, millis);
        }

        setTimeout(update, 0);
    }

    updater(
        // document.getElementById("days"),
        document.getElementById("hours"),
        document.getElementById("minutes"),
        document.getElementById("seconds"),
        document.getElementById("hours2"),
        document.getElementById("minutes2"),
        document.getElementById("seconds2")
    );

    $('.reviews-carousel').owlCarousel({
        loop: true,
        margin: 15,
        nav: false,
        dots: true,
        items: 1,
        center: true,
        autoplay: true,
        autoplayTimeout: 3000
    });


//плавный скролл между якорями
    $('.toform').click(function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        var target = $(href);
        var top = target.offset().top;
        $('html,body').animate({scrollTop: top}, 1000);
    });


    $("select[name='countre']").on('change', function () {
        var price_1 = '',
            price_2 = '';
        if ($(this).val() == 'RU') {
            price_1 = '3198<span class="currency">руб</span>';
            price_2 = '1599<span class="currency">руб</span>';
        } else {
            price_1 = '1798<span class="currency">грн</span>';
            price_2 = '899<span class="currency">грн</span>';

        }
        $('.price-form__old .num').html(price_1);
        $('.price-form__new .num').html(price_2);
    });

    var Pixel = 40,
        myVideo = $('video');

    function checkMedia() {

        var scrollTop = $(window).scrollTop() +Pixel;
        var scrollBottom = $(window).scrollTop() + $(window).height() - Pixel;

        myVideo.each(function (index, el) {
            var yTopMedia = $(this).offset().top;
            var yBottomMedia = $(this).height() + yTopMedia;

            if (scrollTop < yBottomMedia && scrollBottom > yTopMedia) { //view explaination in `In brief` section above
                $(this).get(0).play();
                // console.log("play")
            } else {
                $(this).get(0).pause();
                // console.log("pause")
            }
        });

    }

    $(document).on('scroll', checkMedia);



        $('.data-modal').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            $('' + id + '').show();
            $('#overlay').show();

        });


         $('.close, #overlay').click(function (e) {
            e.preventDefault();
            var id = $(this).attr('href');
            $('.md').hide();
            $('#overlay').hide();

        });

});